///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.h
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Christopher Agcanas <agcanas8@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   29 Jan 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdbool.h>

/// Define the maximum number of cats or dogs in our array-database
#define MAX_SPECIES (20)

enum CatBreed {MAIN_COON, MANX, SHORTHAIR, PERSIAN,SPHYNX};

/// Gender is appropriate for all animals in this database
enum Gender {MALE, FEMALE, NONBINARY};

/// Return a string for the name of the color
enum Color {BLACK, WHITE, RED, BLUE, GREEN, PINK};

char* colorName(enum Color color);

char* genderName(enum Gender gender);

char* breedName(enum CatBreed breed);

char* animalFixed(bool isFixed);
