///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Christopher Agcanas <agcanas8@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   29 Jan 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdbool.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName(enum Color color){
   switch(color){
      case BLACK:
         return "Black";
         break;
      case WHITE:
         return "White";
         break;
      case RED:
         return "Red";
         break;
      case BLUE:
         return "Blue";
         break;
      case GREEN:
         return "Green";
         break;
      case PINK:
         return "Pink";
         break;
      default: 
         return NULL;
               break;
   }
};

// converts enum gender to string
char* genderName(enum Gender gender){
   switch(gender){
      case MALE:
         return "Male";
         break;
      case FEMALE:
         return "Female";
         break;
      case NONBINARY:
         return "Non Binary";
         break;
      default:
         return NULL;
         break;

   }
};

// convert cat breed enum to string
char* breedName(enum CatBreed breed){
   switch(breed){
      case MAIN_COON:
         return "Main Coon";
         break;
      case MANX:
         return "Manx";
         break;
      case SHORTHAIR:
         return "Shorthair";
         break;
      case PERSIAN:
         return "Persian";
         break;
      case SPHYNX:
         return "Sphynx";
         break;
      default: 
         return NULL;
         break;
   };
}

// return yes if fixed and false if not
char* animalFixed(bool isFixed){
   if(isFixed){ 
      return "Yes";
   }
   else if(!isFixed){
      return "No";
   } else {
      return NULL;
   }
} 

